Source: ostree-push
Section: utils
Priority: optional
Maintainer: Andrej Shadura <andrewsh@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 flatpak <!nocheck>,
 gir1.2-ostree-1.0 <!nocheck>,
 pybuild-plugin-pyproject,
 pybuild-plugin-autopkgtest,
 python3-all,
 python3-gi <!nocheck>,
 python3-pytest <!nocheck>,
 python3-setuptools (>= 40.8.0),
 python3-wheel,
 python3-yaml <!nocheck>,
 openssh-client <!nocheck>,
 openssh-server <!nocheck>,
 ostree <!nocheck>,
Standards-Version: 4.6.2
Testsuite: autopkgtest-pkg-pybuild
Homepage: https://github.com/dbnicholson/ostree-push
Vcs-Git: https://salsa.debian.org/debian/ostree-push.git
Vcs-Browser: https://salsa.debian.org/debian/ostree-push
Rules-Requires-Root: no

Package: ostree-push
Architecture: all
Depends:
 gir1.2-ostree-1.0,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Recommends:
 openssh-client,
Suggests:
 flatpak,
 openssh-server,
 ostree,
Description: push commits from local OSTree repo to a remote
 ostree-push uses ssh to push commits from a local OSTree repo to a
 remote OSTree repo. This is to fill a gap where currently you can only
 pull commits in core ostree. To publish commits to a remote repository,
 you either have to pull from the local repo to the remote repo or use
 an out of band mechanism like rsync.
